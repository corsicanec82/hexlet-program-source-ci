FROM node:15.12.0-slim

RUN apt-get update && apt-get install -yq make wget gnupg2

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update \
  && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
    --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*
RUN ln -s /usr/bin/google-chrome-stable /usr/bin/chromium-browser

RUN npm install -g @marp-team/marp-cli
ENV IS_DOCKER true
# TODO: marp использует домашнюю директорию как временную, а npm не использует
# пользователя root. Потому возникает оибка при попытке записи в директорию /root
RUN chmod 777 /root

WORKDIR /app

COPY . .

RUN cd program-handler && npm ci
RUN cd program-handler && npm link
