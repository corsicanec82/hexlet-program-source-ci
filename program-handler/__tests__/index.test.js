// @ts-check

import dirTree from 'directory-tree';
import fse from 'fs-extra';
import path from 'path';
import awsMock from 'aws-sdk-mock';
import nock from 'nock';
import fsp from 'fs/promises';
import tar from 'tar';
import os from 'os';
import fs from 'fs';

import { handleProgram, uploadProgram } from '../index.js';

const sourcePath = path.join('/', 'app');
const mountPath = fs.mkdtempSync(
  path.join(os.tmpdir(), 'test-program-'),
);
const projectName = 'test-programer-program';
const tmpDirPath = path.join(os.tmpdir(), 'test-program');

const options = {
  sourcePath,
  mountPath,
  projectName,
  accessKeyId: 'SOMEKEY',
  secretAccessKey: 'SOMESECRET',
  spaceEndpoint: 'region.example.com',
  spaceName: 'test-data',
  spacePath: 'hexlet-programs',
  permissionLevel: 'public-read',
};

const getArchivePath = (distPath) => path.join(distPath, `${projectName}.tar.gz`);

nock.disableNetConnect();

beforeEach(async () => {
  await fse.remove(tmpDirPath);
  await fsp.mkdir(tmpDirPath);
});

test('handle program', async () => {
  const expectedResponseData = { ETag: '"a3dfb657d626c70fa23086d40a8a45f3"' };
  awsMock.mock(
    'S3',
    'putObject',
    Promise.resolve(expectedResponseData),
  );

  const distPath = await handleProgram(options);
  const archivePath = getArchivePath(distPath);
  await fsp.access(archivePath);
  await tar.x({
    cwd: tmpDirPath,
    file: archivePath,
    noChmod: true,
  });
  expect(dirTree(tmpDirPath)).toMatchSnapshot();

  const responseData = await uploadProgram({ ...options, distPath });
  expect(responseData).toEqual(expectedResponseData);
}, 30000);
