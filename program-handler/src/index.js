// @ts-check
/* eslint no-restricted-syntax: ["off", "ForOfStatement"] */
/* eslint no-await-in-loop: "off" */

import fsp from 'fs/promises';
import fse from 'fs-extra';
import path from 'path';
import debug from 'debug';
import os from 'os';

import { makeArchive, uploadToStorage, getArchivePath } from './utils.js';
import {
  processAction, processMarkers, removeIgnoredFiles, generatePresentation, convertReadmeToHtml,
} from './exerciseHandlers.js';

const log = debug('hexlet');

export const handleProgram = async (options) => {
  const {
    sourcePath,
    projectName,
    mountPath,
  } = options;

  const filesFromDataToCopy = [
    'spec.yml',
  ];

  const distPath = mountPath || await fsp.mkdtemp(
    path.join(os.tmpdir(), 'hexlet-program-'),
  );

  const exercisesPath = path.join(sourcePath, 'exercises');
  const exercisesDistPath = path.join(distPath, 'exercises');

  await fse.copy(exercisesPath, exercisesDistPath);
  await fse.copy(
    path.join(sourcePath, '.gitignore'),
    path.join(distPath, '.gitignore'),
  );

  const promises = filesFromDataToCopy.map((fileName) => (
    fse.copy(
      path.join(sourcePath, '__data__', fileName),
      path.join(distPath, fileName),
    )
  ));
  await Promise.all(promises);

  const exerciseFileNames = await fsp.readdir(exercisesDistPath);
  const exercisePaths = exerciseFileNames.map((exercise) => path.join(exercisesDistPath, exercise));

  // NOTE: when running in parallel, the marp freezes, so we run it sequentially
  for (const exercisePath of exercisePaths) {
    log('generatePresentation', exercisePath);
    await generatePresentation(exercisePath);
  }

  const promisesForProcess = exercisePaths.map(async (exercisePath) => {
    log('copy tutorial to', exercisePath);
    await fse.copy(
      path.join(sourcePath, '__data__', 'TUTORIAL.md'),
      path.join(exercisePath, 'TUTORIAL.md'),
    );
    log('convertReadmeToHtml', exercisePath);
    await convertReadmeToHtml(exercisePath);
    log('processAction', exercisePath);
    await processAction(exercisePath);
    log('removeIgnoredFiles', exercisePath);
    await removeIgnoredFiles(exercisePath);
    log('processMarkers', exercisePath);
    await processMarkers(exercisePath);
  });
  await Promise.all(promisesForProcess);

  log('makeArchive', projectName, distPath);
  await makeArchive(projectName, distPath);

  return distPath;
};

export const uploadProgram = async (options) => {
  const {
    distPath,
    projectName,
  } = options;

  const archivePath = getArchivePath(projectName, distPath);
  log('upload to storage', archivePath);
  const responseData = await uploadToStorage(archivePath, options);

  return responseData;
};
