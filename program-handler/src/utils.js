// @ts-check

import path from 'path';
import fsp from 'fs/promises';
import tar from 'tar';
import AWS from 'aws-sdk';

const getArchiveName = (baseName) => `${baseName}.tar.gz`;

export const getArchivePath = (baseName, sourcePath) => (
  path.join(sourcePath, getArchiveName(baseName))
);

export const makeArchive = async (baseName, sourcePath) => {
  const archivePath = getArchivePath(baseName, sourcePath);
  const items = await fsp.readdir(sourcePath);

  await tar.c({
    gzip: true,
    file: archivePath,
    cwd: sourcePath,
  }, items);

  return archivePath;
};

export const uploadToStorage = async (filePath, options) => {
  const {
    accessKeyId,
    secretAccessKey,
    spaceEndpoint,
    spaceName,
    spacePath,
    permissionLevel,
  } = options;

  const fileName = path.basename(filePath);
  const fileContent = await fsp.readFile(filePath);

  const spacesEndpoint = new AWS.Endpoint(spaceEndpoint);
  const s3 = new AWS.S3({
    endpoint: spacesEndpoint,
    accessKeyId,
    secretAccessKey,
  });

  const params = {
    Bucket: spaceName,
    Key: path.join(spacePath, fileName),
    Body: fileContent,
    ACL: permissionLevel,
  };

  const responseData = await s3.putObject(params).promise();

  return responseData;
};
