#!/usr/bin/env node

import { handleProgram } from '../index.js';

const sourcePath = process.env.SOURCE_PATH;
const mountPath = process.env.MOUNT_PATH;
const projectName = process.env.CI_PROJECT_NAME;

handleProgram({
  sourcePath,
  mountPath,
  projectName,
});
